<?php

/**
 * Winbind server class.
 *
 * @category   apps
 * @package    samba-common
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2009-2012 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/samba_common/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\samba_common;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('samba');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Daemon as Daemon;
use \clearos\apps\base\File as File;
use \clearos\apps\base\Shell as Shell;

clearos_load_library('base/Daemon');
clearos_load_library('base/File');
clearos_load_library('base/Shell');

// Exceptions
//-----------

use \Exception as Exception;

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Winbind server class.
 *
 * @category   apps
 * @package    samba-common
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2009-2012 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/samba_common/
 */

class Winbind extends Daemon
{
    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////

    const FILE_CACHE = '/var/lib/samba/winbindd_cache.tdb';

    /**
     * Winbind constructor.
     *
     * @return void
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);

        parent::__construct('winbind');
    }

    /**
     * Resets the cache.
     *
     * @return void
     * @throws Engine_Exception
     */

    public function reset_cache()
    {
        clearos_profile(__METHOD__, __LINE__);

        // function has been rewritten to avoid base/Daemon calls which rely on pidof calls.
        // These give the wrong results when samba is also running in a docker container.

        // for all the next shell commands
        $options['validate_exit_code'] = FALSE;

        $shell = new Shell();
        if ($shell->Execute('/usr/bin/systemctl', 'is-active winbind -q', TRUE, $options) !=0)
            return;

        $file = new File(self::FILE_CACHE);

        if ($file->exists()) {
            $shell = new Shell();
            $shell->Execute('/usr/bin/systemctl', 'stop winbind', TRUE, $options);

            try {
                $file->delete();
            } catch (Exception $e) {
                // Keep going
            }

            $shell = new Shell();
            $shell->Execute('/usr/bin/systemctl', 'start winbind', TRUE, $options);
        }
    }
}
