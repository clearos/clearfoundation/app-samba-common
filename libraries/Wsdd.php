<?php

/**
 * Samba file and print server class.
 *
 * @category   apps
 * @package    samba-common
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2008-2012 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/samba_common/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\samba_common;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('samba');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Daemon as Daemon;
use \clearos\apps\base\Shell as Shell;

clearos_load_library('base/Daemon');
clearos_load_library('base/Shell');


///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Samba file and print server class.
 *
 * @category   apps
 * @package    samba-common
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2008-2012 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/samba_common/
 */

class Wsdd extends Daemon
{
    /**
     * Wsdd constructor.
     *
     * @return void
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);

        parent::__construct('wsdd');
    }


    /**
     * Returns the running state of wsdd.
     *
     * @return boolean TRUE if wsdd is running
     *
     * @return void
     * @throws Engine_Exception
     */

    public function get_wsdd_running_state()
    {
        clearos_profile(__METHOD__, __LINE__);

        // function has been rewritten to avoid base/Daemon calls which rely on pidof calls.
        // These give the wrong results as wsdd runs as python3 so pidof wsdd never returns anything

        $options['validate_exit_code'] = FALSE;

        $shell = new Shell();
        if ($shell->Execute('/usr/bin/systemctl', 'is-active wsdd -q', FALSE, $options) == 0)
            return TRUE;
        else
            return FALSE;

    }


    /**
     *
     * Starts and enables wsdd.
     *
     */

    public function wsdd_start()
    {
        clearos_profile(__METHOD__, __LINE__);

        // function has been rewritten to avoid base/Daemon calls which rely on pidof calls.
        // These give the wrong results as wsdd runs as python3 so pidof wsdd never returns anything

        $options['validate_exit_code'] = FALSE;

        $shell = new Shell();
        $shell->Execute('/usr/bin/systemctl', 'enable wsdd --now', TRUE, $options);

    }


    /**
     *
     * Stops and disables wsdd.
     *
     */

    public function wsdd_stop()
    {
        clearos_profile(__METHOD__, __LINE__);

        // function has been rewritten to avoid base/Daemon calls which rely on pidof calls.
        // These give the wrong results as wsdd runs as python3 so pidof wsdd never returns anything

        $options['validate_exit_code'] = FALSE;

        $shell = new Shell();
        $shell->Execute('/usr/bin/systemctl', 'disable wsdd --now', TRUE, $options);

    }

}
